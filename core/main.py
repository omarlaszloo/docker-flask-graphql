

from flask import Flask
from flask import jsonify, request

app = Flask(__name__)
app.debug = True

@app.route('/', methods=['GET'])
def index():

    response = {
        'message': 200
    }

    return jsonify(response)

if __name__=='__main__':
    app.run(debug=True, port='5000', host='0.0.0.0')