FROM python:3.6
MAINTAINER "omar.lopez"
ENV PYTHONUNBUFFERED 1
COPY . /core/
WORKDIR /core
RUN pip3 install -r requirements.txt
